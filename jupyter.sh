#!/bin/sh

if [ -z "$GIT_CLONE" ]; then
  echo "nothing to clone"

else
  echo "git repo: $GIT_CLONE"

  basename=$(basename $GIT_CLONE)
  repo=${basename%.*}

  if [ -d "$repo" ]; then
    echo "folder $repo exists (pulling)"
    git -C $repo pull
  else
    echo "folder $repo no found exists (cloning)"
    git clone $GIT_CLONE
  fi

fi
jupyter notebook --port=8888 --no-browser --ip=0.0.0.0
